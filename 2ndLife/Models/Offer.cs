﻿using _2ndLife.Areas.Identity;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _2ndLife.Models
{
    public class Offer
    {

        public int OfferId { get; set; }
        public string OfferName { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public bool IsReported { get; set; }
        public bool IsFree { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public string UserId { get; set; }
        public AppUser User { get; set; }
    }
}
