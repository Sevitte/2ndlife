﻿using _2ndLife.Areas.Identity;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _2ndLife.Models
{
    public class Conversation
    {
        public int ConversationId { get; set; }
        public string UserId { get; set; }
        public string User2ndId { get; set; }

        public List<AppUser> Users { get; set; }

        public List<Message> Messages { get; set; }
    }


}
