﻿using _2ndLife.Areas.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _2ndLife.Models
{
    public class Message
    {
        public int MessageId { get; set; }
        public string MessageContent { get; set; }
        public AppUser User { get; set; }
        public DateTime Time { get; set; }

        public string ConversationId { get; set; }
        public Conversation Conversation { get; set; }
    }
}
