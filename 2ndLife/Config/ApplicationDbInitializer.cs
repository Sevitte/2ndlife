﻿using _2ndLife.Areas.Identity;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _2ndLife.Config
{
        public static class ApplicationDbInitializer
        {

            public static void SeedRoles(RoleManager<IdentityRole> rolesManager)
        {
            if (rolesManager.FindByNameAsync("Admin").Result == null)
            {
                IdentityRole role = new("Admin");

                rolesManager.CreateAsync(role);
            }
        }

            public static void SeedUsers(UserManager<AppUser> userManager)
            {
            

                if (userManager.FindByEmailAsync("abc@xyz.com").Result == null)
                {
                    AppUser user = new AppUser
                    {
                        UserName = "abcd@xyz.com",
                        Email = "abcd@xyz.com"
                    };

                    IdentityResult result = userManager.CreateAsync(user, "Password123!").Result;

                    if (result.Succeeded)
                    {
                        userManager.AddToRoleAsync(user, "Admin").Wait();
                    }
                }
            }
        }
}

