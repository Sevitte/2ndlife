using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml.Linq;
using _2ndLife.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace _2ndLife.Areas.Identity.Pages.Account.Manage
{
    public class LocalizationData : PageModel
    {

        private readonly UserManager<AppUser> _userManager;
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<AppUser> _signInManager;
        public double LocationLen { get; set; }
        public double LocationLat { get; set; }

        public LocalizationData(UserManager<AppUser> userManager, ApplicationDbContext context, SignInManager<AppUser> signInManager)
        {
            _userManager = userManager;
            _context = context;
            _signInManager = signInManager;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            public string UserName { get; set; }
            public string City { get; set; }
            public string Location { get; set; }
            public double LocationLen { get; set; }
            public double LocationLat { get; set; }
        }


        public async Task<IActionResult> OnGet()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            Input = new InputModel
            {
                UserName = user.UserName,
                City = user.City,
                Location = user.Location,
                LocationLen = user.LocationLen,
                LocationLat = user.LocationLat
            };
            LocationLat = user.LocationLat;
            LocationLen = user.LocationLen;


            return Page();
        }

        private async Task LoadAsync(AppUser user)
        {
            var userActual = await _userManager.GetUserAsync(User);
            var userName = await _userManager.GetUserNameAsync(user);
            var userCity = userActual.City;
            var location = userActual.Location;

              Input = new InputModel
               {
                   UserName = userName,
                   City = userCity,
                   Location = location,
                   LocationLen = user.LocationLen,
                   LocationLat = user.LocationLat
              };
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {
                await LoadAsync(user);
                return Page();
            }

            

                user = await _userManager.GetUserAsync(User);
                user.City = Input.City;
                user.Location = Input.Location;

                string requestUri = string.Format("https://maps.googleapis.com/maps/api/geocode/xml?key={1}&address={0}&sensor=false", Uri.EscapeDataString(Input.City +", " + Input.Location), "AIzaSyDaETnkEi81VlxN3DPxWnZeUXg7qjMT3hU");
                WebRequest request = WebRequest.Create(requestUri);
                WebResponse response = request.GetResponse();
                XDocument xdoc = XDocument.Load(response.GetResponseStream());

                XElement result = xdoc.Element("GeocodeResponse").Element("result");
                XElement locationElement = result.Element("geometry").Element("location");
                XElement lat = locationElement.Element("lat");
                XElement lng = locationElement.Element("lng");

                user.LocationLat = double.Parse(lat.Value, CultureInfo.InvariantCulture);
                user.LocationLen = double.Parse(lng.Value, CultureInfo.InvariantCulture);

            await _userManager.UpdateAsync(user);
                //var setPhoneResult = await _userManager.SetPhoneNumberAsync(user, Input.City);
            

            await _signInManager.RefreshSignInAsync(user);
            //StatusMessage = "Your profile has been updated";
            return RedirectToPage();
        }
    }
}
