﻿using _2ndLife.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _2ndLife.Areas.Identity
{
    public class AppUser : IdentityUser
    {
        public string City { get; set; }
        public String Location { get; set; }
        public double LocationLen { get; set; }
        public double LocationLat { get; set; }
        public List<Conversation> Conversations { get; set; }
        public bool isBanned { get; set; }

    }
}
