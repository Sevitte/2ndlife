﻿using _2ndLife.Areas.Identity;
using _2ndLife.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _2ndLife.Services
{
    public interface IConversationData 
    {
        Conversation Get(string User1, string User2, AppUser u1, AppUser u2);

        void Add(Message message);
        void Add(Conversation conversation);
        Conversation Get(int id);
        IEnumerable<Conversation> GetAllConversationByUser(AppUser user1);

    }
}
