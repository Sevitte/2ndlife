﻿using _2ndLife.Areas.Identity;
using _2ndLife.Data;
using _2ndLife.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace _2ndLife.Services
{
    public class SqlConversationData : IConversationData
    {
        private readonly ApplicationDbContext context;
        public SqlConversationData(ApplicationDbContext context)
        {
            this.context = context;
        }

        public void Add(Message message)
        {

            context.Message.Add(message);
            context.SaveChanges();
        }

        public void Add(Conversation conversation)
        {

            var entry = context.Entry(conversation);
            entry.State = EntityState.Modified;
            context.SaveChanges();
        }

        public Conversation Get(string User1, string User2, AppUser u1, AppUser u2)
        {
            Conversation currConv = context.Conversation.Where(conversation => (conversation.UserId == User1 || conversation.User2ndId == User1) 
            && (conversation.User2ndId == User1 || conversation.User2ndId == User2)).Include(o => o.Users).Include(o => o.Messages).FirstOrDefault();
            if (currConv != null)
            {
                if(currConv.Users.Count == 0)
                {
                    currConv.Users.Add(u1);
                    currConv.Users.Add(u2);
                    Add(currConv);
                }

                return currConv;
            }
            else
            {
                List<AppUser> lista = new List<AppUser>();
                lista.Add(u1);
                lista.Add(u2);

                Conversation newConv = new()
                {
                    UserId = User1,
                    User2ndId = User2,
                    Messages = { },
                    Users = lista
                };

                context.Conversation.Add(newConv);
                context.SaveChanges();
                return newConv;
            }           
        }

        public Conversation Get(int id)
        {
            return context.Conversation.Include(o => o.Messages).Include(o => o.Users).FirstOrDefault(o => o.ConversationId == id);
        }

        public IEnumerable<Conversation> GetAllConversationByUser (AppUser user1)
        {
            return context.Conversation.Include(o => o.Users)
                .Include(o => o.Messages)
                .Where(o => o.Users.Contains(user1));
        }
    }
}
