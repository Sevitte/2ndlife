﻿using _2ndLife.Models;
using _2ndLife.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace _2ndLife.Services
{
    public class InMemoryOfferData : IOfferData
    {
        List<Category> categories;
        List<OfferViewModel> offerViews;
        List<Offer> offers;
        public InMemoryOfferData()
        {

            categories = new List<Category>()
            {
                new Category { CategoryId = 1, CategoryName = "Kosmetyki" },
                new Category { CategoryId = 2, CategoryName = "Sprzęt elektroniczny" },
                new Category { CategoryId = 3, CategoryName = "Zwierzęta" }
            };

            offers = new List<Offer>()
            {
                new Offer { OfferId = 1, OfferName = "Kot", IsFree = true },
                new Offer { OfferId = 2, OfferName = "Pies", IsFree = true },
                new Offer { OfferId = 3, OfferName = "Szczotka", IsFree = true }
            };


        }
        public void Add(OfferViewModel offer, string User)
        {
            offerViews.Add(offer);
            offer.OfferId = offerViews.Max(r => r.OfferId) + 1;
        }

        public void Ban(int id)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            var offer = Get(id);
            if (offer != null)
                offers.Remove(offer);
        }

        public List<Offer> filteredCategory(int? CategoryId)
        {
            throw new NotImplementedException();
        }

        public Offer Get(int id)
        {
            return offers.FirstOrDefault(r => r.OfferId == id);
        }

        public IEnumerable<Offer> GetAll()
        {
            return offers.OrderBy(r => r.OfferName);
        }

        public IEnumerable<Category> GetAllCategories()
        {
            throw new NotImplementedException();
        }

        public Category GetCategory(int id)
        {
            throw new NotImplementedException();
        }

        public OfferViewModel GetViewModel(int id)
        {
            throw new NotImplementedException();
        }

        public void Report(int id)
        {
            throw new NotImplementedException();
        }

        public void Unreport(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(OfferViewModel offer)
        {
            var existing = Get(offer.OfferId);
            if(existing != null)
            {
                existing.OfferName = offer.OfferName;
                existing.IsFree = offer.IsFree;
                existing.Description = offer.Description;
            }
        }
        public IEnumerable<Offer> Find(Expression<Func<Offer, bool>> expression)
        {
            throw new NotImplementedException();
        }
    }
}
