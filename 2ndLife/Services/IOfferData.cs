﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;
using _2ndLife.Models;
using _2ndLife.ViewModels;

namespace _2ndLife.Services
{
    public interface IOfferData
    {
        IEnumerable<Offer> GetAll();
        IEnumerable<Category> GetAllCategories();
        IEnumerable<Offer> Find(Expression<Func<Offer, bool>> expression);
        List<Offer> filteredCategory(int? CategoryId);
        Category GetCategory(int id);
        Offer Get(int id);
        OfferViewModel GetViewModel(int id);
        void Add(OfferViewModel offer, string User);
        void Update(OfferViewModel offer);
        void Delete(int id);
        void Report(int id);
        void Unreport(int id);
        void Ban(int id);
    }
}
