﻿using _2ndLife.Models;
using _2ndLife.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace _2ndLife.Services
{
    public class SqlCategoryData : ICategoryData
    {
        private readonly ApplicationDbContext db;
        public SqlCategoryData(ApplicationDbContext db)
        {
            this.db = db;
        }
        public void Add(Category category)
        {
            db.Category.Add(category);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            var category = db.Category.Find(id);
            db.Category.Remove(category);
            db.SaveChanges();
        }

        public Category Get(int id)
        {
            return db.Category.FirstOrDefault(c => c.CategoryId == id);
        }

        public IEnumerable<Category> GetAll()
        {
            return from c in db.Category
                   orderby c.CategoryName
                   select c;
        }

        public void Update(Category category)
        {
            var entry = db.Entry(category);
            entry.State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
