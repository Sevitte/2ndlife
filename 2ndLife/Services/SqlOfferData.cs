﻿using _2ndLife.Areas.Identity;
using _2ndLife.Data;
using _2ndLife.Models;
using _2ndLife.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;

namespace _2ndLife.Services
{
    public class SqlOfferData : IOfferData
    {
        private readonly ApplicationDbContext db;
        private readonly IWebHostEnvironment webHostEnvironment;
        private readonly UserManager<AppUser> _userContext;

        public SqlOfferData(ApplicationDbContext db, IWebHostEnvironment hostEnvironment, UserManager<AppUser> userManager)
        {
            this.db = db;
            webHostEnvironment = hostEnvironment;
            _userContext = userManager;
        }
        public void Add(OfferViewModel offerView, string User)
        {
            //var user = User;
            string imgName = UploadedImg(offerView);
            AppUser user = _userContext.FindByIdAsync(User).Result;

            Offer offer = new ()
            {
                OfferName = offerView.OfferName,
                Description = offerView.Description,
                Image = imgName,
                IsReported = offerView.IsReported,
                IsFree = offerView.IsFree,
                CategoryId = offerView.CategoryId,
                Category = offerView.Category,
                UserId = User,
                User = user
            };

            db.Offer.Add(offer);
            db.SaveChanges();
        }

        public List<Offer> filteredCategory(int? CategoryId)
        {
            if (CategoryId == null)
            {
                return db.Offer.Include(o => o.Category).Include(o => o.User).ToList();
            }
            else
            {
                return db.Offer.Include(o => o.Category).Include(o => o.User).Where(o => o.CategoryId == CategoryId).ToList();
            }
        }

        public void Delete(int id)
        {
            var offer = db.Offer.Find(id);
            db.Offer.Remove(offer);
            db.SaveChanges();
        }

        public Offer Get(int id)
        {
            return db.Offer.Include(o => o.User).FirstOrDefault(o => o.OfferId == id);
        }

        public OfferViewModel GetViewModel(int id)
        {


            Offer offer = Get(id);
            OfferViewModel offerVM = new()
            {
                OfferId = offer.OfferId,
                Description = offer.Description,
                Category = offer.Category,
                CategoryId = offer.CategoryId,
                IsFree = offer.IsFree,
                IsReported = offer.IsReported,
                OfferName = offer.OfferName,
                User = offer.User,
                UserId = offer.UserId
                //dodac stare zdjecie 
            };
            return offerVM;
        }

        public Category GetCategory(int id)
        {
            return db.Category.FirstOrDefault(c => c.CategoryId == id);
        }

        public IEnumerable<Category> GetAllCategories()
        {

           // return db.Category;

            return from c in db.Category
                   select c;
        }
        public IEnumerable<Offer> GetAll()
        {
            return from c in db.Category
                   join o in db.Offer on c.CategoryId equals o.CategoryId
                   where c.CategoryId == o.CategoryId
                   orderby o.OfferName
                   select o;
        }

        public void Update(OfferViewModel offerView)
        {
            string imgName = UploadedImg(offerView);

            Offer prevOffer = db.Offer.Include(o => o.User).FirstOrDefault(o => o.OfferId == offerView.OfferId);

            prevOffer.OfferName = offerView.OfferName;
            prevOffer.Description = offerView.Description;
            prevOffer.Image = imgName;
            prevOffer.IsReported = offerView.IsReported;
            prevOffer.IsFree = offerView.IsFree;
            prevOffer.CategoryId = offerView.CategoryId;
            prevOffer.Category = offerView.Category;
            prevOffer.User = prevOffer.User;
            prevOffer.UserId = prevOffer.UserId;

            db.SaveChanges();
        }


        public void Report(int i)
        {
            Offer offer = db.Offer.Find(i);
            offer.IsReported = true;

            db.SaveChanges();
        }

        public void Unreport(int i)
        {
            Offer offer = db.Offer.Find(i);
            offer.IsReported = false;

            db.SaveChanges();
        }

        public void Ban(int id)
        {
            Offer offer = db.Offer.Include(o => o.User).Where(o => o.OfferId == id).FirstOrDefault();
            offer.User.isBanned = true;

            db.SaveChanges();
        }

        private string UploadedImg(OfferViewModel offerView)
        {
            string fileName = null;

            if (offerView.Image != null)
            {
                string uploadFolder = Path.Combine(webHostEnvironment.WebRootPath, "userimage");
                fileName = Guid.NewGuid().ToString() + offerView.Image.FileName;
                string filePath = Path.Combine(uploadFolder, fileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    offerView.Image.CopyTo(fileStream);
                }
            }
            return fileName;
        }
        public IEnumerable<Offer> Find(Expression<Func<Offer, bool>> expression) 
        {
            return db.Set<Offer>().Where(expression);
        }
    }
}
