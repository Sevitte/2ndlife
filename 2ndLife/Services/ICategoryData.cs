﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using _2ndLife.Models;

namespace _2ndLife.Services
{
    public interface ICategoryData
    {
        IEnumerable<Category> GetAll();
        Category Get(int id);
        void Add(Category category);
        void Update(Category category);
        void Delete(int id);
    }
}
