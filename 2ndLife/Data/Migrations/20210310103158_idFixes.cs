﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace _2ndLife.Data.Migrations
{
    public partial class idFixes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Offername",
                table: "Offer",
                newName: "OfferName");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Offer",
                newName: "OfferId");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Category",
                newName: "CategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "OfferName",
                table: "Offer",
                newName: "Offername");

            migrationBuilder.RenameColumn(
                name: "OfferId",
                table: "Offer",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "CategoryId",
                table: "Category",
                newName: "Id");
        }
    }
}
