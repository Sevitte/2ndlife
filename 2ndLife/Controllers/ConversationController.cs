﻿using _2ndLife.Areas.Identity;
using _2ndLife.Models;
using _2ndLife.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _2ndLife.Controllers
{
    public class ConversationController : Controller
    {

        private readonly IConversationData context;
        private readonly UserManager<AppUser> userContext;

        public ConversationController(IConversationData context, UserManager<AppUser> userContext)
        {
            this.context = context;
            this.userContext = userContext;
        }

        public IActionResult Index(int id)
        {
            Conversation conv = context.Get(id);
            return View(conv);
        }

        [HttpPost]
        public async Task<IActionResult> Index(string User1, string User2)
        {
            AppUser u1 = await userContext.GetUserAsync(User);
            AppUser u2 = await userContext.FindByIdAsync(User2);
            
            Conversation conversation = context.Get(User1, User2, u1, u2);

            return View(conversation);
        }

        [HttpPost]
        public ActionResult Create(string messageContent, int id)
        {
            //if(String.IsNullOrEmpty(restaurant.Name))
            //{
            //    ModelState.AddModelError(nameof(restaurant.Name), "The name is required");
            //}

            Conversation conv = context.Get(id);
            Message message = new() {
                Conversation = conv,
                MessageContent = messageContent,
                ConversationId = id.ToString(),
                Time = DateTime.Now,
                User = userContext.GetUserAsync(User).Result
            };
            if(conv.Messages == null)
            {
                conv.Messages = new List<Message>();
            }
            
            conv.Messages.Add(message);
            //context.Add(message);
            context.Add(conv);

            if (ModelState.IsValid)
            {
                //db.Add(offer);
                return View("Index", conv);
            }
            return View("Index", conv);
        }

        public IActionResult All()
        {
            IEnumerable<Conversation> conversations = context.GetAllConversationByUser(userContext.GetUserAsync(User).Result);
            return View(conversations);
        }


    }
}
