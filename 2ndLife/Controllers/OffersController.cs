﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using _2ndLife.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using _2ndLife.ViewModels;
using _2ndLife.Areas.Identity;
using _2ndLife.Services;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace _2ndLife.Controllers
{
    public class OffersController : Controller
    {
        private readonly IOfferData db;
        private readonly UserManager<AppUser> userContext;

        public OffersController(IOfferData db, UserManager<AppUser> userContext)
        {
            this.db = db;
            this.userContext = userContext;
        }

        public IActionResult Index(int? CategoryId)
        {
            ViewData["CategoryId"] = new SelectList(db.GetAllCategories(), "CategoryId", "CategoryName", "Select");
            List<Offer> list = db.filteredCategory(CategoryId);
            return View(list);
        }
        
        [Authorize]
        public IActionResult Details(int id)
        {
            var offer = db.Get(id);
            
            ViewBag.User1 = userContext.GetUserId(User).ToString();
            ViewBag.User2 = offer.UserId.ToString();

            var category = db.GetCategory(offer.CategoryId);
            offer.Category = category;
            if(offer == null || category == null)
            {
                return View("NotFound");
            }
            return View(offer);
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var u = await userContext.GetUserAsync(User);
            if (!u.isBanned)
            {
                ViewBag.CategoryId = new SelectList(db.GetAllCategories(), "CategoryId", "CategoryName");
                return View();
            }
            else
            {
                return View("NotAuthorized");
            }
        }

        //model binding
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(OfferViewModel offer)
        {
            ViewBag.CategoryId = new SelectList(db.GetAllCategories(), "CategoryId", "CategoryName");

            if (ModelState.IsValid)
            {
                db.Add(offer, userContext.GetUserId(User));
                return RedirectToAction("Index");
            }
            
            return View();
        }

        
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var u = await userContext.GetUserAsync(User);
            if (!u.isBanned) { 

                var model = db.GetViewModel(id);
            if (userContext.GetUserId(User).ToString() == model.UserId)
            {
            ViewBag.CategoryId = new SelectList(db.GetAllCategories(), "CategoryId", "CategoryName");
            if (model == null)
            {
                return NotFound();
            }
            return View(model);
        }
            else
                return View("NotFound");
            }
            else
            {
                return View("NotAuthorized");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(OfferViewModel offer)
        {
            if (ModelState.IsValid)
            {
                db.Update(offer);
                return RedirectToAction("Details", new { id = offer.OfferId });
            }
            return View();
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            var model = db.Get(id);
            if(model == null )
            {
                return View("NotFound");
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(int id, IFormCollection form)
        {
            db.Delete(id);
            return RedirectToAction("Index");
        }

        public IActionResult Report(int id)
        {
            db.Report(id);

            return RedirectToAction("Index");
        }

        public IActionResult Unreport(int id)
        {
            db.Unreport(id);

            return RedirectToAction("Index");
        }

        public IActionResult Ban(int id)
        {
            db.Ban(id);

            return RedirectToAction("Index");
        }



    }
}
