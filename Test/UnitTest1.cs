using System;
using Xunit;
using _2ndLife.Services;
using _2ndLife.Models;
using _2ndLife.ViewModels;
using _2ndLife.Controllers;
using System.Collections.Generic;

namespace Test
{
    public class UnitTest1
    {

        public IOfferData tdb;
        
        void InitializeTestDatabase()
        {
            tdb = new InMemoryOfferData();
            string user = "user";
            var kotV = new OfferViewModel { OfferId = 1, OfferName = "Kot", IsFree = true };
            var piesV = new OfferViewModel { OfferId = 2, OfferName = "Pies", IsFree = true };
            tdb.Add(kotV, user);
            tdb.Add(piesV, user);
        }

        [Fact]
        public void DisposeTestDatabase()
        {
            tdb = null;
        }
        [Fact]
        public void Test()
        {
            var a = 1;
            var b = 2;
            Assert.True(a < b);
        }

        [Fact]
        public void TestAdd()
        {
            InitializeTestDatabase();
            OfferViewModel offerView = new OfferViewModel { OfferId = 1, OfferName = "Kot", IsFree = true };
            Assert.Equal(tdb.Get(0).OfferName, offerView.OfferName);
            DisposeTestDatabase();
        }

        [Fact]
        public void TestGet()
        {
            InitializeTestDatabase();
            var offer = tdb.Get(1);
            string kot = "Kot";
            Assert.Equal(offer.OfferName, kot);
            DisposeTestDatabase();
        }

        [Fact]

        public void TestDelete()
        {
            InitializeTestDatabase();
            tdb.Delete(1);
            Assert.Null(tdb.Get(1));
            DisposeTestDatabase();
        }

        [Fact]
        public void TestUpdate()
        {
            InitializeTestDatabase();
            var offer = new OfferViewModel { OfferId = 1, IsFree = false, Description = "brak opisu", OfferName = "Kotek" };
            tdb.Update(offer);
            string kotek = "Kotek";
            Assert.Equal(tdb.Get(1).OfferName, kotek);
            DisposeTestDatabase();
        }
    }
}
